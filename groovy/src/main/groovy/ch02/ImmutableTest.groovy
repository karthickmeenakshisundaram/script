import groovy.transform.Immutable;
@Immutable class Atom{
    float weight
}

def atom=new Atom(10);

assert atom.weight == 10

try{
atom.weight=15
assert false , "I reset the atom weight to 15. This shouldn't have been possible because an atom is immutable"
}catch(ReadOnlyPropertyException e){
    println "Exception caught. Yes. Atom is immutable now"
}
