package ch08

/**
 * Created by karthick on 22/03/17.
 */
class Car {

    int maxSpeed
    int currentSpeed
    String model
    String yearOfMake

    public boolean canAccelerate(){
        return currentSpeed < maxSpeed;
    }

    public void applyBrakes(){
        println "Brakes applied on $model-$yearOfMake"
    }

    def methodMissing(String methodName, Object arguments){
        if(methodName=='isAntiBrakingSystemAvailable'){
            return false;
        }
    }

    def propertyMissing(String propertyName){
        "$propertyName doesnt exist!! sorry"
    }

}

