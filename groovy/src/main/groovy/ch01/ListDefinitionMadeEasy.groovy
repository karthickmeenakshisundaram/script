println "Arrays in java vs List in Groovy"

list=[1, 2, 3] //you could declare an array in java as new Integer[]{1,2,3}; but you wouldn't be able to add to it.
list.add 4;
println list
list.remove 1 // you are removing at index 1 which would be the number 2.
println list
