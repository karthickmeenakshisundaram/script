import org.apache.commons.lang.ClassUtils
@Grab('commons-lang:commons-lang:2.4')

class Outer{
    class Inner{
    
    }
}

assert ClassUtils.isInnerClass(Outer.Inner)
println "Complete";