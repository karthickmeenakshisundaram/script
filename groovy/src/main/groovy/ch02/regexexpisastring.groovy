
def abc="123456"

assert abc =~ /\d+/

def regex=/\d+/;
def justonedigitregex=/\d/

assert abc.replaceAll(justonedigitregex, "*") == '******'

