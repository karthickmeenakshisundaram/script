import groovy.transform.Immutable;

@Immutable class Car{
	int mileage
	float price
	int speed
	String name
	
}
def cars= [new Car(10,1000000,100, 'Swift'),new Car(10,1000000,100, 'Swift'),new Car(12,800000,80, 'Ritz')]

class Fastest<Car> implements Comparator<Car>{
	
	int compare(Car a, Car b){
		return a.speed-b.speed;
	}
	
}

def fastest=new Fastest<Car>();

class Buyer<T>{
	
	Comparator<T> comp;
	
	Buyer buy(Comparator<T> comp){
		this.comp=comp
		this
	}
	
	T of(List<T> ts){
		def ArrayList<T> ts1=new ArrayList<T>(ts)
		Collections.sort(ts1,comp)
		ts1.remove(ts1.size()-1);
	}
	
	
}
Buyer<Car> buyer=new Buyer<Car>();
print buyer.buy(fastest).of(cars).name;
print buyer.buy(fastest) of cars name;