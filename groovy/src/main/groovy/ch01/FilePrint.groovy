println "Explicit brackets for method parameter"
println "======================================"
new File('HelloWorld.groovy').eachLine({ line -> println line })
println "======================================"
println ""
println ""

println "brackets Implicit for method parameter"
println "======================================"
new File('HelloWorld.groovy').eachLine{ line -> println line }
println "======================================"
println ""
println "both are same."
