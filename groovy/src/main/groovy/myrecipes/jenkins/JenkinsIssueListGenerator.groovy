package jenkins

import groovy.lang.Grab;
@Grab('org.apache.commons:commons-csv:1.2')
import org.apache.commons.csv.CSVParser
import org.apache.commons.csv.CSVPrinter

import java.util.regex.Matcher;

import org.apache.commons.csv.CSVFormat;

import groovy.json.JsonOutput
import groovy.json.JsonSlurper


class JenkinsIssueListGen{
	//Delimiter used in CSV file
	private static final String NEW_LINE_SEPARATOR = "\n";
	//CSV file header
	private static final Object [] FILE_HEADER = [
		"Ticket No",
		"Info Ticket",
		"Patch No",
		"Build Version",
		"Revision",
		"Author Name",
		"Modified Files",
		"Date",
		"comment"
	];

	def urlString;


	JenkinsIssueListGen(def jenkinsProjectUrl, def buildnumber){
		urlString="${jenkinsProjectUrl}/${buildnumber}/api/json"
	}

	JenkinsIssueListGen(def urlString){
		this.urlString=urlString;
	}


	void printCSV(def fileName){
		FileWriter fileWriter = null;
		CSVPrinter csvFilePrinter = null;

		//Create the CSVFormat object with "\n" as a record delimiter
		CSVFormat csvFileFormat = CSVFormat.DEFAULT.withRecordSeparator(NEW_LINE_SEPARATOR);

		//initialize FileWriter object
		fileWriter = new FileWriter(fileName);

		//initialize CSVPrinter object
		csvFilePrinter = new CSVPrinter(fileWriter, csvFileFormat);
		//Create CSV file header
		csvFilePrinter.printRecord(FILE_HEADER);

		def build=new JsonSlurper().parse(new URL(urlString));
		def buildVersion=getBuildVersion(build);

		build.changeSet.items.each { item ->
			if(item.author.fullName != 'jenkins'){
				csvFilePrinter.printRecord([
					getDefectId(item) ,
					"",
					"",
					buildVersion,
					item.commitId,
					item.author.fullName,
					item.affectedPaths.join("\n"),
					new Date(item.timestamp),
					item.comment
				])
			}
		}

		fileWriter.flush();
		fileWriter.close();
		csvFilePrinter.close();
	}

	def getBuildVersion(def build){
		def currentReleaseVersion="";
		build.changeSet.items.each { item ->
			if(currentReleaseVersion == ''){
				if(item.author.fullName == 'jenkins'){
					def m;
					if ( (m = item.comment =~ /Committing all version updates for ([0-9]{1,4}\.[0-9]{1,4}\.[0-9]{1,4}\.[0-9]{1,4})(-SNAPSHOT)/ )){
						currentReleaseVersion= m.group(1);
					}
				}
			}
		}
		return currentReleaseVersion;
	}

	def getDefectId(def item){
		//(item.comment =~ /.*?([A-za-z]{2,4}-[0-9{4,5}]).*/) ?.group(1)
		def m;
		if((m= item.comment =~ /([A-za-z]{2,4}-[0-9]{4,5}).*/)){
			return m.group(1);
		}	else{
			println "didnt match"
		}
	}

	def testGetBuildVersion(){
		def build=new JsonSlurper().parse(new URL(urlString));
		println getBuildVersion(build);
	}

	def testGetDefectId(){
		assert "FUTS-12345" == getDefectId(['comment':"FUTS-12345 blah blah"])
	}

}


println JsonOutput.prettyPrint(JsonOutput.toJson(build));

//"http://localhost:8080/job/VendorCentralReporting"
def BUILD_NUMBER='49';
//def url="http://localhost:8080/job/VendorCentralReporting/${BUILD_NUMBER}/api/json"
def urlString="file:///home/karthick/sample.json"
//println System.getProperties();
new JenkinsIssueListGen("file:///${System.properties['user.dir']}/testdata/sample.json").testGetDefectId()
//printCSV("issuelist.csv")

//testGetBuildVersion();
//printCSV("issuelist.csv");