customers=new XmlSlurper().parse(new File('testdata/customers.xml'));
for ( customer in customers.corporate.customer) {
    println "${customer.@name}  works for   ${customer.@company}"
}

for ( customer in customers.consumer.customer) {
    println "${customer.@name}  lives at   ${customer.@address}"
}

println customers.noofcustomers

println "Complete"

