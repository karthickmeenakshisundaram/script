import ch08.Car
import org.junit.Test
import static org.junit.Assert.*

/**
 * Created by karthick on 22/03/17.
 */
class CarTest {

    @Test
    void testMissingMethodOnCar(){
        Car car=new Car(maxSpeed:100,currentSpeed:50,model:'Maruti',yearOfMake:'2016');
        assert car.canAccelerate()
        assertFalse(car.isAntiBrakingSystemAvailable());
        assert car.abcd=='abcd doesnt exist!! sorry'
    }

    @Test
    void testMissingPropertyOnCar(){
        Car car=new Car(maxSpeed:100,currentSpeed:50,model:'Maruti',yearOfMake:'2016');
        assert car.canAccelerate()
        assert car.abcd=='abcd doesnt exist!! sorry'
    }

}
